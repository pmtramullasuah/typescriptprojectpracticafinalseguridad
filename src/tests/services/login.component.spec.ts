import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { UserService } from '@athena/core/services/user.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core'
import { LoginComponent } from '../../app/login/login.component';
import { Router } from '@angular/router';
import { Title, EventManager } from '@angular/platform-browser';
import { EventHandlerVars } from '@angular/compiler/src/compiler_util/expression_converter';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  //Crear mocks de servicios
  let userServiceSpy: jasmine.SpyObj<UserService>;
  let routerServiceSpy: jasmine.SpyObj<Router>;
  let titleServiceSpy: jasmine.SpyObj<Title>;

  beforeEach(async(() => {
    //Creacion de mocks
    const spyUserService = jasmine.createSpyObj('UserService', ['signIn']);
    const spyRouter = jasmine.createSpyObj('Router', ['navigate']);
    const spyTitle = jasmine.createSpyObj('Title', ['setTitle']);
    
    TestBed.configureTestingModule({
      imports: [FormsModule, BrowserAnimationsModule],
      declarations: [ LoginComponent ],
      providers: [
        { provide: UserService, useValue: spyUserService },
        { provide: Router, useValue: spyRouter },
        { provide: Title, useValue:spyTitle }
      ],
      schemas:[NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    userServiceSpy = TestBed.get(UserService);
    routerServiceSpy = TestBed.get(Router);
    titleServiceSpy = TestBed.get(Title);

    //Preparar funciones de los mocks
    titleServiceSpy.setTitle.and.returnValue(null);
  });

  it('instancia y loading a false a cargar', () => {
    expect(component).toBeTruthy();
    expect(component.loading).toBeFalsy();
  });

  it('formulario solo se muestra cuando loading es false', () => {
    const dom = fixture.nativeElement;
    let form: HTMLFormElement;
    //El formulario se debe mostrar cuando loading sea false
    component.loading = false;
    fixture.detectChanges();
    form = dom.querySelector('#formulario_login')
    expect(form).toBeTruthy();
    //El formulario se debe esconder cuando loading sea true
    component.loading = true;
    fixture.detectChanges();
    form = dom.querySelector('#formulario_login')
    expect(form).toBeNull();
  });

  it('mostrar error del mensaje cuando ocurre un error', ()=>{
    const dom = fixture.nativeElement;
    let message: HTMLDivElement;
    //No debe mostrarse ningun mensaje cuando no hay errores
    component.errorSubmit = 'Error'
    fixture.detectChanges();
    message = dom.querySelector('#errorM')
    expect(message).toBeTruthy();
  })

  it('loading when User submit and route', async(() => {
    const dom = fixture.nativeElement;
    let form: HTMLFormElement = dom.querySelector("#formulario");

    //Preparar Funciones
    userServiceSpy.signIn.and.returnValue(Promise.resolve())
    const navigateSpy=routerServiceSpy.navigate.and.returnValue(null);

    //Simular evento para enseñar loading
    form.dispatchEvent(new Event('ngSubmit'));
    expect(component.loading).toBeTruthy();

    //Resolucion promesa y redireccion
    fixture.whenStable()
      .then(()=>{
        expect(navigateSpy.calls.count()).toBe(1)
      })
  }))

  it('show error', async(()=> {
    const error = {'message':'Error'};
    const dom = fixture.nativeElement;
    let form: HTMLFormElement = dom.querySelector("#formulario");

    //Preparar Funciones
    userServiceSpy.signIn.and.returnValue(Promise.reject(error))

    //Simular evento para enseñar loading
    form.dispatchEvent(new Event('ngSubmit'));
    expect(component.loading).toBeTruthy();

    //Resolucion promesa y redireccion
    fixture.whenStable()
      .then(()=>{
        fixture.detectChanges();
        expect(component.loading).toBeFalsy();
        expect(component.errorSubmit).toBeTruthy();
      })
  }))
});
