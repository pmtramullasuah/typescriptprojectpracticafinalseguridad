// Si existe un padre, es que el módulo ya ha sido cargado.
export function throwIfAlreadyLoaded(parentModule: any, moduleName: string) {
    if (parentModule) {
        throw new Error(
            `${moduleName} is already loaded. Import it in the AppModule only`);
    }
}
