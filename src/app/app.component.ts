import { Component, OnInit, HostListener } from '@angular/core';
import { MenuComponent } from './core/menu/menu.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Athena-Visiotech';
}
