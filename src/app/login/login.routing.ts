// Importar modulos del router de angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Importar componentes
import { LoginComponent } from './login.component';

// Declarar rutas
const routes: Routes = [
	{
		path: '',
		component: LoginComponent,
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class LoginRoutingModule {}
