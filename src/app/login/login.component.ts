import { Component, OnInit } from '@angular/core';
import { CognitoService } from '@athena/core/services/cognito.service';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { trigger, transition, useAnimation } from '@angular/animations';
import { fadeIn } from 'ng-animate';

import { UserService } from '@athena/core/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  animations: [
    trigger('fadeIn', [transition('* => *', useAnimation(fadeIn))])
  ]
})
export class LoginComponent implements OnInit {
  fadeIn: any;
  public title = 'athena-client';
  public loading: boolean; // Atributo para controlar la vista

  public userName = '';
  public userPassword = '';

  public tokenJson = null;

  public errorSubmit: string;

  constructor(private _userService: UserService,
              private _router: Router,
              private _titleService: Title) {
              this.loading = false; // Vista para mostrar el formulario
  }

  ngOnInit() {
    this._titleService.setTitle('Login | Athena');
  }

  onSubmit() {
    this.loading = true; // Vista para indicar que se esta cargando la informacion
    this._userService.signIn(this.userName, this.userPassword)
      .then(() => {
        this._router.navigate(['/athena']);
      })
      .catch((error) => {
        this.errorSubmit = error.message;
        this.loading = false;
      });
  }
}
