export * from './shared.module';
export * from './pipes/file-size.pipe';
export * from './material.module';
export * from './models/archivo.model';
export * from './models/file.model';
export * from './models/tipofichero.model';
