export interface Categoria {
    id:string,
    name:string,
    published:string,
    url:string
}