export interface tableSearch {
    activeSort: string;
    directionSort: string;
    productSearch: string;
    stringSearch: string;
    pageIndex: number;
    pageSize: number;
    typeFilter: string;
}