export const tipofichero: string[] = ['ficha fabricante', 'firmware', 'manual', 'guia', 'folleto', 'software', 'certificado'];
export const sistemasOperativos: string[] = ['Windows', 'Linux', 'MacOS', 'Android', 'iOS'];
export const estados: string[] = ['borrador', 'publicado', 'despublicado'];
export const accesos: string[] = ['publico', 'privado'];
export const idiomas: string[] = ['ES', 'FR', 'IT', 'EN', 'PT', 'DE'];

