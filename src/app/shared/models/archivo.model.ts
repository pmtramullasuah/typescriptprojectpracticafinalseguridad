export class ArchivoModel {
    constructor(
        public ruta: string,
        public size: string,
        public date,
        public etag: string,
        public downloadURL: string,
    ) {}
}
