import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { FileSizePipe } from './pipes/file-size.pipe';
import { MaterialModule } from './material.module';


@NgModule({
  imports: [],
  declarations: [
    FileSizePipe
  ],
  exports: [
    MaterialModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FileSizePipe
  ]
})
export class SharedModule { }
