import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'; // Boostrap css

import { FileRoutingModule } from './file.routing';
import { FileComponent } from './file.component';
import { EditComponent, DialogDeleteContent } from './edit/edit.component';
import { DialogSameHashFileComponent } from './elements/dialog-same-hash-file/dialog-same-hash-file.component';
import { SearchComponent } from './search/search.component';
import { SharedModule } from '@athena/shared';
@NgModule({
  imports: [
    CommonModule,
    AngularEditorModule,
    HttpClientModule,
    NgbModule,
    SharedModule,
    FileRoutingModule
  ],
  declarations: [
    EditComponent,
    DialogDeleteContent,
    FileComponent,
    SearchComponent,
    DialogSameHashFileComponent
  ],
  entryComponents: [
    DialogDeleteContent,
    DialogSameHashFileComponent
  ]
})
export class FileModule { }
