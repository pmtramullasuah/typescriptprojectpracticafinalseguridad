import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogSameHashFileComponent } from './dialog-same-hash-file.component';

describe('DialogSameHashFileComponent', () => {
  let component: DialogSameHashFileComponent;
  let fixture: ComponentFixture<DialogSameHashFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogSameHashFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSameHashFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
