import { Component, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

export interface DialogData {
  rutaAthena: string;
}

@Component({
  selector: 'app-dialog-same-hash-file',
  templateUrl: './dialog-same-hash-file.component.html',
  styleUrls: ['./dialog-same-hash-file.component.css']
})
export class DialogSameHashFileComponent {

  constructor(public dialogRef: MatDialogRef<DialogSameHashFileComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
      this.dialogRef.disableClose = true;
     }

  openURL(){
    console.log(this.data.rutaAthena)
    window.open(this.data.rutaAthena)
  }

  closeClick(){
    this.dialogRef.close();
  }

}
