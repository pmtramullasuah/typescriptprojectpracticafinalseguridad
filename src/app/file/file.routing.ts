import { EditAuthGuard } from '../core/guards/edit-auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditComponent } from './edit/edit.component';
import { SearchComponent } from './search/search.component';

const routes: Routes = [
  {
    path: '',
    component: SearchComponent,
  },
  {
    path: 'edit/:id',
    component: EditComponent,
    canActivate: [EditAuthGuard]
  },
  {
    path: 'edit',
    component: EditComponent,
    canActivate: [EditAuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FileRoutingModule { }
