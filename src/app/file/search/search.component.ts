import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import {
    MatPaginator,
    MatSort,
    MatTableDataSource,
    MatSnackBar,
    MatSortable,
    Sort,
    PageEvent,
    SortDirection
} from '@angular/material';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { combineAll, take,takeUntil,filter, map, max, pluck, startWith } from 'rxjs/operators';

import { CognitoService } from '@athena/core/services/cognito.service';
import { UserService } from '@athena/core/services/user.service';
import { PeticionesService } from '@athena/core/services/peticiones.service';
import { IconService } from "@athena/core/services/icon.service";
import { DataRepositoryService } from "@athena/core/services/data-repository.service";

import { tipofichero } from '@athena/shared/models/tipofichero.model';
import { AthenaService } from '@athena/core';
import { Title } from '@angular/platform-browser';
import { directiveCreate } from "@angular/core/src/render3/instructions";
import { bool } from 'aws-sdk/clients/signer';
import { Categoria } from "@athena/shared/models/categoria.model";
import { tableSearch } from "@athena/shared/models/tableSearch.model";

export interface Firmware {
    name: string;
    products: string;
    version: string;
}

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

    public types = tipofichero;
    public columnsToDisplay: string[] = ['titulo', 'version', 'fileType', 'fileState',
        'fabricante', 'marca', 'creacion', 'fileDownload', 'fileCompatibles'];

    myDataArray = new MatTableDataSource();
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    // Autocomplete
    myControl = new FormControl();
    options: string[] = ['One', 'Two', 'Three'];
    filteredOptions: Observable<string[]>;
    productsName: string[] = [];
    productsApi: any[];

    // Variables ordenacion, filtros defecto
    defaultTableSearch = {
        activeSort: 'titulo',
        directionSort: 'asc',
        productSearch: '',
        stringSearch: '',
        pageIndex: 0,
        pageSize: 10,
        typeFilter: ''
    };
    // Select
    selectControl = new FormControl();

    //Campo busqueda libre;
    stringBusqueda = ""

    // Carga
    cargandoProductos = false;
    cargandoCategorias = false;

    // Almacén de campos de la tabla
    tableSearch: tableSearch;

    // Edit
    satUserLoggedIn: boolean;

    // Observables
    products$: Observable<any>;

    constructor(private _peticionesService: PeticionesService,
        private _athenaService: AthenaService,
        private _userService: UserService,
        private _router: Router,                  // Redirección de vistas
        private route: ActivatedRoute,              // Operaciones sobre la ruta actual
        private snackBar: MatSnackBar,              // Mensajes de confirmacion
        private _titleService: Title,                // Título
        public iconService: IconService,         // Servicio compartido de iconos
        public dataRepository: DataRepositoryService // Servicio de almacén de datos

    ) {
        this.tableSearch = this.defaultTableSearch;
        this.myDataArray.filterPredicate = (data: any, filter: string) => {
            return data.tipo === filter.toLowerCase();
        };
        this.products$ = this.dataRepository.productos$.pipe(filter(result=>result!=null)).pipe(take(1));
    }

    ngOnInit() {

        /*this.myDataArray.data = [{
            titulo:"Holaprimo",
            version:"version",
            tipo:"tipo",
            acceso:"acceso",
            estado:"estado",
            fabricante:"fabricante",
            marcas:["marca"],
            descarga:"",
            compatibles:"",
            archivo:{
                ruta:"google.es"
            },
            productos:["p1","p2","p3"],
            categorias: [1,2,3]
    
        },
            {
                titulo:"Holaprimo2",
                version:"version",
                tipo:"tipo",
                acceso:"acceso",
                estado:"estado",
                fabricante:"fabricante",
                marcas:["marca"],
                descarga:"",
                compatibles:"",
                archivo:{
                    ruta:"google.es"
                },
                productos:["p1","p2","p3"],
                categorias: [1,2,3]
    
            },
            {
                titulo:"Holaprimo3",
                version:"version",
                tipo:"tipo",
                acceso:"acceso",
                estado:"estado",
                fabricante:"fabricante",
                marcas:["marca"],
                descarga:"",
                compatibles:"",
                archivo:{
                    ruta:"google.es"
                },
                productos:["p1","p2","p3"],
                categorias: [1,2,3]
    
            },
            {
                titulo:"Holaprimo4",
                version:"version",
                tipo:"tipo",
                acceso:"acceso",
                estado:"estado",
                fabricante:"fabricante",
                marcas:["marca"],
                descarga:"",
                compatibles:"",
                archivo:{
                    ruta:"google.es"
                },
                productos:["p1","p2","p3"],
                categorias: [1,2,3]
    
            }];*/

        // Título
        this._titleService.setTitle('Busqueda | Athena');

        this.dataRepository.getProductos();
        this.dataRepository.getCategorias();


        this.products$.subscribe(
            (result) => {
                console.log("Recibo los productos");
                if (result) { // Esperamos a que el resultado tenga algún valor, hasta que no tenga va a ser nulo
                    // JSON to Array string names
                    this.productsName = result.map(value => value.name);
                    this.productsApi = result;

                    // Una vez que los resultados son cargados se asigna el cambio de valor del input a una función
                    this.filteredOptions = this.myControl.valueChanges
                        .pipe(
                            startWith(''),
                            map(value => this._filter(value))
                        );

                    // Cogemos los parametros del session storage y lo cargamos en la variable local
                    if (sessionStorage.getItem('tableSearch')) {
                        this.tableSearch = JSON.parse(sessionStorage.getItem('tableSearch'));
                        if (this.tableSearch.productSearch!='') {
                            // Realizamos la búsqueda

                            this.productSelected(this.tableSearch.productSearch, false);
                            this.myControl.setValue(this.tableSearch.productSearch);

                            /*//Paginacion
                            //Asignamos el paginador por defecto
                            this.myDataArray.paginator = this.paginator;
                            let paginator = this.myDataArray.paginator;
                            paginator.pageIndex=this.tableSearch.pageIndex;
                            paginator.pageSize=this.tableSearch.pageSize;
                            this.myDataArray.paginator = paginator;*/
                        }else if(this.tableSearch.stringSearch!=''){
                            // Realizamos la búsqueda
                            this.stringBusqueda = this.tableSearch.stringSearch;
                            console.log(this.stringBusqueda)
                            this.busquedaLibre(this.tableSearch.stringSearch, true);
                        } else {
                            this.myDataArray.paginator = this.paginator;
                            this.myDataArray.sort = this.sort;
                        }
                    } else {
                        this.myDataArray.paginator = this.paginator;
                        this.myDataArray.sort = this.sort;
                    }
                }
            }
        );

        this.satUserLoggedIn = this._userService.isSatUser();
    }

    filtroBusqueda(value) {
        this.myDataArray.filter = value;
        this.tableSearch.typeFilter = value;
        this.actualizaSession();
    }

    /*cargaProductos() {
        this.cargandoProductos=true;
        this.dataRepository.getProductos()
            .then((result:Array<any>)=>{
                // JSON to Array string names
                this.productsName = result.map(value => value.name);

                this.productsApi = result;

                // Una vez que los resultados son cargados se asigna el cambio de valor del input a una función
                this.filteredOptions = this.myControl.valueChanges
                    .pipe(
                        startWith(''),
                        map(value => this._filter(value))
                    );

                this.cargandoProductos=false;
            })
            .catch((error)=>{
                console.error(error);
            });
    }*/

    private _filter(value: string): string[] {
        const filterValue = value.toLowerCase();
        let contador = 0;
        const maxResults = 100;
        if (filterValue.length > 1) {
            return this.productsName.filter(option => {
                if (contador < maxResults && option.toLowerCase().includes(filterValue)) {
                    contador++;
                    return true;
                } else {
                    return false;
                }
            });
        }
    }
    /*cargaCategorias() {
        this.cargandoCategorias=true;
        this.dataRepository.getCategorias()
            .then((result:Array<Categoria>)=>{
                this.cargandoCategorias=false;
            })
            .catch((error)=>{
                console.error(error);
            });
    }*/

    productSelected(product, resetSearch) {
        if (resetSearch) {
            this.tableSearch = this.defaultTableSearch;
        }
        // Guardamos la busqueda en la variable
        this.tableSearch.productSearch = product;
        this.tableSearch.stringSearch = '';
        this.tableSearch.activeSort = 'titulo';
        this.tableSearch.directionSort = 'asc';

        //Borramos la busqueda libre si habia anteriormente
        this.stringBusqueda="";

        //Marcamos la ordenacion
        this.sort.active = this.tableSearch.activeSort;
        this.sort.direction = <SortDirection>this.tableSearch.directionSort;
        this.myDataArray.sort = this.sort;

        //Filtrado
        this.myDataArray.filter = this.tableSearch.typeFilter;
        this.selectControl.setValue(this.tableSearch.typeFilter);

        //Asignamos el paginador por defecto
        this.myDataArray.paginator = this.paginator;

        //Guardamos en sesión los nuevos valores
        this.actualizaSession();

        let categories = this.findCategoriesProduct(product);
        this._athenaService.getFileJSONByProduct(product, categories).pipe(pluck('hits', 'hits')).subscribe(
            (result: Array<any>) => {

                this.myDataArray.data = result.map(value => { return Object.assign({}, value._source, { id: value._id }) });

                /* paginator.pageIndex=this.tableSearch.pageIndex;
                 paginator.pageSize=this.tableSearch.pageSize;
                 //paginator.length = result.length;
                 this.myDataArray.paginator = paginator;
                 */
                this.myDataArray.paginator.pageIndex = this.tableSearch.pageIndex;
                this.myDataArray.paginator.pageSize = this.tableSearch.pageSize;
                this.myDataArray._updateChangeSubscription();


            },
            error => { debugger }
        )
    }

    busquedaLibre(string, resetSearch){
        if (resetSearch) {
            this.tableSearch = this.defaultTableSearch;
        }
        //Quitamos espacios en blanco
        var consulta = string.trim();

        // Guardamos la busqueda en la variable
        this.tableSearch.productSearch = '';
        this.tableSearch.stringSearch = consulta;
        this.tableSearch.activeSort = '';
        this.tableSearch.directionSort = '';

        //Borramos la busqueda de producto si habia anteriormente
        this.myControl.setValue(this.tableSearch.productSearch);

        //Marcamos la ordenacion
        this.sort.active = this.tableSearch.activeSort;
        this.sort.direction = <SortDirection>this.tableSearch.directionSort;
        this.myDataArray.sort = this.sort;
    

        //Filtrado
        this.myDataArray.filter = this.tableSearch.typeFilter;
        this.selectControl.setValue(this.tableSearch.typeFilter);

        //Asignamos el paginador por defecto
        this.myDataArray.paginator = this.paginator;

        //Guardamos en sesión los nuevos valores
        this.actualizaSession();

        
        this._athenaService.getFileByAnyField(consulta).pipe(pluck('hits', 'hits')).subscribe(
            (result: Array<any>) => {

                this.myDataArray.data = result.map(value => { return Object.assign({}, value._source, { id: value._id }) });

                /* paginator.pageIndex=this.tableSearch.pageIndex;
                 paginator.pageSize=this.tableSearch.pageSize;
                 //paginator.length = result.length;
                 this.myDataArray.paginator = paginator;
                 */
                this.myDataArray.paginator.pageIndex = this.tableSearch.pageIndex;
                this.myDataArray.paginator.pageSize = this.tableSearch.pageSize;
                this.myDataArray._updateChangeSubscription();


            },
            error => { debugger }
        )

    }
    productBorrador(resetSearch) {

        if (resetSearch) {
            
            this.tableSearch = this.defaultTableSearch;
        }
        // Guardamos la busqueda en la variable
        this.tableSearch.productSearch = "";

        //Marcamos la ordenacion
        this.sort.active = this.tableSearch.activeSort;
        this.sort.direction = <SortDirection>this.tableSearch.directionSort;
        this.myDataArray.sort = this.sort;

        //Filtrado
        this.myDataArray.filter = this.tableSearch.typeFilter;
        this.selectControl.setValue(this.tableSearch.typeFilter);

        //Asignamos el paginador por defecto
        this.myDataArray.paginator = this.paginator;

        //Guardamos en sesión los nuevos valores
        this.actualizaSession();

        this._athenaService.getBorradorFile().pipe(pluck('hits', 'hits')).subscribe(
            (result: Array<any>) => {

                this.myDataArray.data = result.map(value => { return Object.assign({}, value._source, { id: value._id }) });

                /* paginator.pageIndex=this.tableSearch.pageIndex;
                 paginator.pageSize=this.tableSearch.pageSize;
                 //paginator.length = result.length;
                 this.myDataArray.paginator = paginator;
                 */
                this.myDataArray.paginator.pageIndex = this.tableSearch.pageIndex;
                this.myDataArray.paginator.pageSize = this.tableSearch.pageSize;
                this.myDataArray._updateChangeSubscription();
                console.log(this.myDataArray.paginator);


            },
            error => { debugger }
        )
    }

    copyText(file) {
        if (file.archivo['ruta'] !== '' || this.hasHref(file.descripcionLarga)) {
            const selBox = document.createElement('textarea');
            selBox.style.position = 'fixed';
            selBox.style.left = '0';
            selBox.style.top = '0';
            selBox.style.opacity = '0';
            if(file.archivo['ruta'] !== ''){
                selBox.value = 'https://athena-visiotech.s3-eu-west-1.amazonaws.com/' + file.id + '/' + encodeURIComponent(file.archivo['ruta']);
            }else{
                selBox.value = this.extractHref(file.descripcionLarga);
            }
            document.body.appendChild(selBox);
            selBox.focus();
            selBox.select();
            document.execCommand('copy');
            document.body.removeChild(selBox);
            this.snackBar.open('Enlace de descarga copiado al portapapeles', '', {
                duration: 2000
            });
        }else {
            alert('No hay ningun archivo asociado, revise el contenido')
        }
    }


    extractHref(desc){​​
        let regex = /href=\"(.*?)\"/;
        if (regex.test(desc)){​​
            let extract = regex.exec(desc);
            return extract[1];
        }​​
        else{​​
            return "#";
        }​​
    }

    hasHref(desc){​​
        let regex = /href=\"(.*?)\"/;
        return regex.test(desc);
    }​​

    

    downloadArchivo(file) {
        if (file.archivo['ruta'] !== '') {
            window.open('https://athena-visiotech.s3-eu-west-1.amazonaws.com/' + file.id + '/' + encodeURIComponent(file.archivo['ruta']), '_self');
        } else {
            alert('No hay ningun archivo asociado, revise el contenido')
        }
    }

    findCategoriesProduct(product) {
        let productFiltered = this.productsApi.filter(item => item.name == product);
        if (productFiltered) { //Si por alguna razón no encuentra productos
            return productFiltered[0].categories.map(item => { return +item });
        }
        else {
            return [];
        }
    }


    /**
     * Detecta cambio en la ordenación de la tabla y almacena los datos en variables locales
     * @param sort
     */
    onSortTableChanged(sort: Sort) {
        this.tableSearch.activeSort = sort.active;
        this.tableSearch.directionSort = sort.direction;
        this.actualizaSession();

    }

    /**
     * Detecta cambio en la paginacion de la tabla y almacena los datos en variables locales
     * @param page
     */
    onPageTableChanged(page: PageEvent) {
        this.tableSearch.pageIndex = page.pageIndex;
        this.tableSearch.pageSize = page.pageSize;
        this.actualizaSession();
    }

    /*
     * Actualiza la variable de sesión que almacena las variables de la tabla
     */
    actualizaSession() {
        sessionStorage.setItem('tableSearch', JSON.stringify(this.tableSearch));
    }
}
