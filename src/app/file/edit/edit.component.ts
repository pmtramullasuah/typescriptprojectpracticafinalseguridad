import { Component, ElementRef, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { sistemasOperativos, tipofichero, estados, accesos, idiomas } from '@athena/shared/models/tipofichero.model';
import { FormControl, FormsModule } from '@angular/forms';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { FileModel } from '@athena/shared/models/file.model';
import {Categoria} from "@athena/shared/models/categoria.model";
import { ArchivoModel } from '@athena/shared/models/archivo.model';
import { AthenaService } from '@athena/core/services/athena.service';
import { S3Service } from '@athena/core/services/s3.service';
import { PeticionesService } from '@athena/core/services/peticiones.service';
import { map, startWith } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ViewChild  } from '@angular/core';
import { CognitoService } from '@athena/core/services/cognito.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Title } from '@angular/platform-browser';
import { trigger, transition, useAnimation } from '@angular/animations';
import { fadeIn, flash, fadeInRight } from 'ng-animate';
import {IconService} from "@athena/core/services/icon.service";
import { UserService } from '@athena/core/services/user.service';
import {DataRepositoryService} from "@athena/core/services/data-repository.service";
import {DialogSameHashFileComponent} from '../elements/dialog-same-hash-file/dialog-same-hash-file.component';
import { reject } from 'q';
import { HashService } from '@athena/core';
import { strictEqual } from 'assert';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'file-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css'],
    animations: [
        trigger('fadeIn', [transition('void => *', useAnimation(fadeIn))]),
        trigger('flash', [transition('* => *', useAnimation(flash))]),
        trigger('fadeInRight', [transition('* => *', useAnimation(fadeInRight,
            {
                params: { timing: 0.3 }
            }))])

    ]
})
export class EditComponent implements OnInit {

  // Atributos para mostrar en la vista
  public tipos = tipofichero;
  public sistemasOperativosList = sistemasOperativos;
  public estadosList = estados;
  public accesosList = accesos;
  public idiomasList = idiomas;
  public fabricantes: string[] = [];
  public marcas: string[] = [];
  public productos: string[] = [];
  public productos_completo = [];

  // public categorias: string[] = [];
  public categorias_api_web: Array<Categoria> = [];
  public categorias_selected_local : Array<Categoria> = [];


  // Atributos para controlar el estado del fichero de la vista
  public archivo: ArchivoModel = null;          // Modelo para controlar el estado del archivo asociado
  public fichero: FileModel = new FileModel();    // Modelo para mantener estado del fichero

  // Elementos del controlador
  controlFabricante = new FormControl();
  filteredFabricante: Observable<string[]>;

  controlMarca = new FormControl();
  filteredMarca: Observable<string[]>;

  controlProductos = new FormControl();
  filteredProductos: Observable<string[]>;

  controlCategorias = new FormControl();
  filteredCategorias: Observable<any[]>;

    //cARGA
    cargandoFichero= false;
    enviandoDatos=false;

    //Animations
    fadeIn: any;
    flash: any;
    fadeInRight:any;

    //Variables error
    errorGuardado: string;

    //Observables de información
    private marcas$ : Observable<String[]>;

  @ViewChild('inputFile') myInputVariable: ElementRef;

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '4rem',
    placeholder: '',
    translate: 'no',
    uploadUrl: 'v1/images', // if needed
    customClasses: [ // optional
    {
      name: 'quote',
      class: 'quote',
    },
    {
      name: 'redText',
      class: 'redText'
    },
    {
      name: 'titleText',
      class: 'titleText',
      tag: 'h1',
    }
    ]
  };

  constructor(
    private _athenaService: AthenaService,    // Acceso a servicios de Athena-Visiotech
    private _s3Service: S3Service,            // Acceso a servicios de S3
    private _apiVisiotech: PeticionesService, // Acceso a servicios de API-Visiotech
    private _route: ActivatedRoute,           // Acceso a los parametros de la ruta
    private _router: Router,                  // Redirección de vistas
    private _loginService: CognitoService, // Controlar la autentificacion del usuario
    private _userService: UserService,          // Controlar la autentificacion del usuario
    public snackBar: MatSnackBar,              // Mensajes de confirmacion
    private _titleService: Title,                //Título
    private _deleteDialog: MatDialog,              //Dialogo
    public iconService: IconService,           //Servicio compartido de iconos
    public dataRepository: DataRepositoryService, //Servicio de almacén de datos
    private _hashService: HashService,           //Servicio para crear Hash de los objetos y comprobar duplicados en S3.
    public dialog: MatDialog                    //Referencia para controlar mensaje de aviso si se introducen archivos duplicados 
  ) {
  }

  ngOnInit() {
      this._titleService.setTitle('Edición | Athena');

      
              this.dataRepository.marcas$.subscribe(
                  (result)=>{
                      this.marcas = result;
                  }
              );

              this.dataRepository.fabricantes$.subscribe(
                  (result)=>{
                      if (result){
                          this.fabricantes = result;
                          this.filteredFabricante = this.controlFabricante.valueChanges
                              .pipe(
                                  startWith(''),
                                  map(value => this._filterFabricantes(value))
                              )
                      }
                  }
              );
              this.dataRepository.categorias$.subscribe(
                  (result)=>{
                      if (result){ //Esperamos a que el resultado tenga algún valor, hasta que no tenga va a ser nulo
                          this.categorias_api_web = result;
                          this.filteredCategorias = this.controlCategorias.valueChanges
                              .pipe(
                                  startWith(''),
                                  map(value => this._filterCategorias(value))
                              );
                          this.categorias_selected_local = this.dataRepository.apiCategoriesToLocal(this.fichero.categorias)
                      }
                  }
              );
              this.dataRepository.productos$.subscribe(
                  (result)=>{
                      if (result){ //Esperamos a que el resultado tenga algún valor, hasta que no tenga va a ser nulo
                          // JSON to Array string names
                          this.productos = result.map(value => value.name);
                          this.productos_completo = result;

                          // Una vez que los resultados son cargados se asigna el cambio de valor del input a una función
                          this.filteredProductos = this.controlProductos.valueChanges
                              .pipe(
                                  startWith(''),
                                  map(value => this._filterProductos(value))
                              );

                      }
                  }
              );

              // Si se ha cargado /edit/:id, la vista pasara a forma de edicion de fichero y cargara los campos con la informacion del fichero
            this._route.paramMap.subscribe((result: ParamMap) => {
                  if (result.has('id')) {
                      this.getFicheroData(result.get('id'));
                  }else{ //Llamamos al resto de servicios para obtener informacion
                      this.dataRepository.getMarcas();
                      this.dataRepository.getFabricantes();
                      this.dataRepository.getProductos();
                      this.dataRepository.getCategorias();
                  }
                });

            if (this.fichero.tipo === 'firmware') {
              this.fichero.titulo = 'FW - ' + this.fichero.fabricante + ' - ' + this.fichero.version;
            }
      
  }

  // Metodo para actualizar el estado del archivo asociado, (campo de URL se borrara, archivo no asociado a S3)
  onFileChanged(event) {

    this.enviandoDatos = true;

    //Comprobacion de que el archivo no exista en el S3
    this._hashService.getHash(this.myInputVariable).then((hash: string)=>{
      this._athenaService.getEtag(hash).subscribe((result)=>{
        this.enviandoDatos = false;
        if(result['hits']['total']!=0){
          var fichero_aux = result['hits']['hits'][0];
          this.mostrarAvisoHashDuplicado('https://athena.visiotechsecurity.com/#/athena/edit/'+fichero_aux['_id'])
          
        }else{
            this.archivo = new ArchivoModel(event.target.files[0].name, event.target.files[0].size, event.target.files[0].lastModified, hash,'');
        }
      },(err)=>{
        this.enviandoDatos = false;
        console.log('error al obtener hashes de otros productos')
      })
    }).catch((err)=>{
      console.log(err)
    })
  }

  //Metodo para mostrar mensaje de aviso cuando se introduzca un archivo duplicado
  mostrarAvisoHashDuplicado(ruta){
    const dialogRef = this.dialog.open(DialogSameHashFileComponent, {
      width: '550px',
      data: {rutaAthena: ruta}
    })

    dialogRef.afterClosed().subscribe(()=>{
      this.myInputVariable.nativeElement.value = '';
    })

    
  }

  // Metodo para actualizar o subir la informacion del fichero
  onSubmit() {
    if (!this.fichero.IDFile) { // Se trata de un fichero nuevo a subir
      this.subirFichero();
    } else {
      this.actualizarFichero(); // Se trata de un fichero existente a actualizar
    }
  }

  // Metodo para ejecutar fichero
  subirFichero() {
    this.enviandoDatos = true;

    this.fichero.archivo['ruta'] = '';
    this.fichero.archivo['tamanno'] = '';
    this.fichero.archivo['fechaModificacion'] = '';
    this.fichero.archivo['etag'] = '';

    this.fichero.categorias = [];
    for(let categoria of this.categorias_selected_local){
        this.fichero.categorias.push(+categoria.id);
    }

      this.fichero.fechaCreacion = ""+Date.now();
      this.fichero.fechaPublicacion = ""+Date.now();

      this._athenaService.uploadFileJSON(this.fichero.toJson())  // Subida fichero JSON a Athena
      .then((request: any) => {
        request.subscribe(
          result => {
            console.log(result);
            this.fichero.IDFile = result.data._id;        // Registrar ID del nuevo fichero a File Model
            if (this.archivo) {              // Si se ha asociado un archivo, se actualiza el fichero con la informacion y se sube al S3
              this.actualizarArchivoS3()
                .then(()=>{
                  this.setS3Info();
                  this.flujoActualizacion(); //Se actualiza el fichero con la información de S3
                })
                .catch((err)=>{
                  this.enviandoDatos=false;
                  this.errorGuardado = 'Error al actualizar S3, el fichero no tiene asociado un fichero';
                  console.log('Error al actualizar S3, el fichero no tiene asociado un fichero');
                })
            } else {
                this.enviandoDatos = false;

                const snackBarReference = this.snackBar.open('Cambios guardados correctamente', '', {
                    duration: 2000
                });

                snackBarReference.afterDismissed().subscribe(
                    result => {this._router.navigate(['/athena']);},
                    error1 => {console.log("error")}
                );

              /*alert('Archivo actualizado correctmente');
              this._router.navigate(['/athena']);*/
            }

          },
          error => {
              this.enviandoDatos=false;
              this.errorGuardado = error;
            console.log('Error Athena-Visiotech', error);  // Error en la peticion a Athena-Visiotech
          }
        );
      })
      .catch((err) => {
        console.log(err);  // Error al crear la promesa

        if (err === 401) {

          this._router.navigate(['/login']);  // Error de autentificacion, se envia a la pagina de login
        }
      });
  }

  actualizarFichero() {
    console.log(this.fichero.toJson());
    this.enviandoDatos=true;

    this.fichero.categorias = [];
    for(let categoria of this.categorias_selected_local){
        this.fichero.categorias.push(+categoria.id);
    }

    this.fichero.fechaPublicacion = ""+Date.now();

    const oldS3 = this.fichero.archivo['ruta'];
    this.setS3Info();

    // Si se ha seleccionado borrar (y antes existia un archivo) o hay un nuevo archivo, se actualiza el S3
    if (((!this.archivo) && (oldS3 !== '')) || ((this.archivo) && (this.archivo.downloadURL === ''))) {
      this.actualizarArchivoS3()
      .then(()=>{
          this.flujoActualizacion();
      })
      .catch((err)=>{
        this.fichero.archivo['ruta'] = '';
        this.fichero.archivo['tamanno'] = '';
        this.fichero.archivo['fechaModificacion'] = '';
        this.fichero.archivo['etag'] = '';

        this.flujoActualizacion(true);
      })
    }
    else{
      this.flujoActualizacion();
    }
  }

  flujoActualizacion(errorS3=false){
    this._athenaService.updateFileJSON(this.fichero.toJson()) // Actualizacion del fichero JSON a Athena
      .then((request: any) => {
        request.subscribe(
          (result) => {
            // Si se ha seleccionado borrar (y antes existia un archivo) o hay un nuevo archivo, se actualiza el S3
                if(!errorS3){
                  this.enviandoDatos=false;

                const snackBarReference = this.snackBar.open('Cambios guardados correctamente', '', {
                    duration: 2000
                });

                snackBarReference.afterDismissed().subscribe(
                    result => {this._router.navigate(['/athena']);},
                    error1 => {console.log("error")}
                );
                }
                else{
                  this.enviandoDatos=false;
                  this.errorGuardado = 'Error al actualizar S3, el fichero no tiene archivo asociado';
                }


              //this._router.navigate(['/athena']);
            
          },
          error => {
              this.enviandoDatos=false;
              this.errorGuardado = 'Error al actualizar Athena';
            console.log('Error Athena-Visiotech', error); // Error en la peticion a Athena-Visiotech
          });
      })
      .catch((err) => {
        console.log(err);  // Error al crear la promesa
        if (err === 401) {
          this._router.navigate(['/login']);  // Error de autentificacion, se envia a la pagina de login
        }
      });
  }

  borrarFichero() {

      let dialogoBorrado =  this._deleteDialog.open(
          DialogDeleteContent,
          {}
      );
      dialogoBorrado.afterClosed().subscribe(result => {
          if(result){
              this.enviandoDatos=true;
              console.log(this.fichero.toJson());

              const data = {
                  'id': this.fichero.IDFile,
                  'tipo': this.fichero.tipo,
              };

              this._athenaService.deleteFileJSON(data) // Borrar el fichero de ES y S3
                  .then((request: any) => {
                      request.subscribe(
                          (result) => {
                              console.log(result);
                              this.enviandoDatos=false;
                              const snackBarReference = this.snackBar.open('Fichero borrado correctamente', '', {
                                  duration: 2000
                              });

                              snackBarReference.afterDismissed().subscribe(
                                  result => {this._router.navigate(['/athena']);},
                                  error1 => {console.log(error1)}
                              );
                          },
                          (error) => {
                              this.enviandoDatos=false;
                              this.errorGuardado = error;
                              console.log('Error Athena-Visiotech', error); // Error en la peticion a Athena-Visiotech
                          }
                      );
                  })
                  .catch((err) => {
                      console.log(err);  // Error al crear la promesa
                      if (err === 401) {
                          this._router.navigate(['/login']); // Error de autentificacion, se envia a la pagina de login
                      }
                  });
          }
      });

  }

  actualizarArchivoS3() {

    // Si no hay ningun archivo asociado, se pasara null al segundo parametro
    let aux = null;
    if (this.myInputVariable.nativeElement.files.length !== 0) {
      aux = this.myInputVariable.nativeElement.files[0]; // Existe un archivo asociado
    }

    return new Promise((resolve,reject) => {
      this._s3Service.updateFileS3(this.fichero.IDFile, aux)
      .then((data) => {
        console.log('Todo correcto'); // Actualizacion correcta
          resolve()

      })
      .catch((err) => {
        console.log('Error S3: ', err); // Error en la actualizacion del S3
        if (err === 401) {
          this._router.navigate(['/login']); // Error de autentificacion, se envia a la pagina de login
      }
      reject(err)
      });
    })
  }

  setS3Info() {
    if (!this.archivo) {
      this.fichero.archivo['ruta'] = '';
      this.fichero.archivo['tamanno'] = '';
      this.fichero.archivo['fechaModificacion'] = '';
      this.fichero.archivo['etag'] = '';
    } else {
      this.fichero.archivo['ruta'] = this.archivo.ruta;
      this.fichero.archivo['tamanno'] = String(this.archivo.size);
      this.fichero.archivo['fechaModificacion'] = String(this.archivo.date);
      this.fichero.archivo['etag'] = this.archivo.etag;
    }
  }

  /*cargaFabricantes() {
      this.dataRepository.getFabricantes()
          .then((result:Array<string>)=>{
              this.fabricantes = result;
              this.filteredFabricante = this.controlFabricante.valueChanges
                  .pipe(
                      startWith(''),
                      map(value => this._filterFabricantes(value))
                  );})
          .catch((error)=>{
              console.error(error);
          });
  }*/

/*  cargaMarcas() {
      this.dataRepository.getMarcas()
          .then((result:Array<string>)=>{
              this.marcas = result;
              this.filteredMarca = this.controlMarca.valueChanges
                  .pipe(
                      startWith(''),
                      map(value => this._filterMarcas(value))
                  );})
          .catch((error)=>{
              console.error(error);
          });
  }*/

  /*cargaCategorias() {

      this.dataRepository.getCategorias()
          .then((result:Array<Categoria>)=>{
              this.categorias_api_web = result;
              this.filteredCategorias = this.controlCategorias.valueChanges
                  .pipe(
                      startWith(''),
                      map(value => this._filterCategorias(value))
                  );
              this.categorias_selected_local = this.dataRepository.apiCategoriesToLocal(this.fichero.categorias);})
          .catch((error)=>{
              console.error(error);
          });
  }*/

  private _filterCategorias(value:string): Categoria[] {
      // Cuando llama a optionSelected el value del campo pasa a ser el valor del campo seleccionado
      // que es un objeto y da problemas, significa que ya ha seleccionado y ponemos el array completo
      if (typeof(value) == "string") {
          const filterValue = value.toLowerCase();
          return this.categorias_api_web.filter(option => option.name.toLowerCase().includes(filterValue));
      }else{
          return this.categorias_api_web;
      }
    }

  private _filterMarcas(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.marcas.filter(option => option.toLowerCase().includes(filterValue));
  }

  private _filterFabricantes(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.fabricantes.filter(option => option.toLowerCase().includes(filterValue));
  }



  getFicheroData(id) {
      this.cargandoFichero = true;
    // Obtener informacion del fichero para actualizar
    this._athenaService.getFileJSONById(id).subscribe(
      result => {
        const aux = result['hits'].hits[0]; // Respuesta del endpoint ElasticSearch
        if (aux) {                          // Se ha encontrado un fichero con el ID aportado
          this.fichero.set(aux._source, aux._id); // Actualizar FileModel con la informacion del fichero (con ID de ES)
          // Si tiene archivos asociados, se actualiza ArchivoModel con la informacion del archivo
          if (this.fichero.archivo['ruta'] !== '') {
            this.archivo = new ArchivoModel(this.fichero.archivo['ruta'], this.fichero.archivo['tamanno'],
              this.fichero.archivo['fechaModificacion'], this.fichero.archivo['etag'],'https://athena-visiotech.s3-eu-west-1.amazonaws.com/' + this.fichero.IDFile + '/'
              + encodeURIComponent(this.fichero.archivo['ruta'])); // Actualizar informacion de ArchivoModel
          }
          //Una vez que hemos recibido toda la información del fichero, cargamos todo lo de la api.
            this.dataRepository.getMarcas();
            this.dataRepository.getFabricantes();
            this.dataRepository.getProductos();
            this.dataRepository.getCategorias();
            this.cargandoFichero = false;

        } else { // No existe ficheros con ese ID, se envia a la pagina principal
          this._router.navigate(['/athena']);
        }

      },
      error => { console.log('Error ElasticSearch', error); }
    );
  }

  /*cargaProductos() {
    this.cargandoProductos=true;
    this.dataRepository.getProductos()
    .then((result:Array<any>)=>{
        // JSON to Array string names
        this.productos = result.map(value => value.name);
        this.productos_completo = result;

      // Una vez que los resultados son cargados se asigna el cambio de valor del input a una función
      this.filteredProductos = this.controlProductos.valueChanges
          .pipe(
              startWith(''),
              map(value => this._filterProductos(value))
          );

      this.cargandoProductos=false;
    })
    .catch((error)=>{
      console.error(error);
    });
  }*/



  private _filterProductos(value: string): string[] {
    if (value) {
      const filterValue = value.toLowerCase();
      let contador = 0;
      const maxResults = 100;
      if (filterValue.length > 1) {
        return this.productos.filter(option => {
          if (contador < maxResults && option.toLowerCase().includes(filterValue)) {
            contador++;
            return true;
          } else {
            return false;
          }
        });
      }
    }
  }

  // Actualizar la vista como subida de un nuevo fichero con los datos anteriores
  duplicarFichero() {
    this.fichero.IDFile = null;     // La vista cambia a subir nuevo archivo
    this.fichero.titulo = this.fichero.titulo + ' - Copia';
    this.archivo = null;            // Limpiar ArchivoModel
    this.myInputVariable.nativeElement.value = ''; // Limpiar archivo asociado
    
    history.pushState({ page: 'athena/edit' }, '', 'athena/edit');
 }

  //Controlamos cuando se hace click sobre el nombre en vez de sobre el check
  productClicked(event: Event, product) {
        event.stopPropagation();
        this.toggleProductSelection(product);
    }
  categorieClicked(event: Event, categorie) {
        event.stopPropagation();
        this.toggleCategorieSelection(categorie);
    }

    //Añadimos o borramos del array de productos/categorias seleccionados en función de si existe o no
    toggleProductSelection(product) {
      const productPosition = this.fichero.productos.indexOf(product);
      if (productPosition == -1){
          this.fichero.productos.unshift(product);
      }else{
          this.fichero.productos.splice(productPosition, 1);
      }
    }
    toggleCategorieSelection(categorie) {
      const categoriePosition = this.categorias_selected_local.indexOf(categorie);
      if (categoriePosition == -1){
          this.categorias_selected_local.unshift(categorie);
      }else{
          this.categorias_selected_local.splice(categoriePosition, 1);
      }
    }

    //Buscamos en el array de productos relacionados si existe o no el producto/categoria por parámetro
    productRelatedExists(product):boolean {
        return (this.fichero.productos.indexOf(product) !== -1)
    }
    categorieRelatedExists(categorie):boolean {
        return (this.categorias_selected_local.indexOf(categorie) !== -1)
    }

  deleteRelatedProduct(i) {
    this.fichero.productos.splice(i, 1);
  }


  deleteRelatedCategorie(i) {
    this.categorias_selected_local.splice(i, 1);
  }

  borrarArchivo() {
    // Borrar el archivo asociado
    this.archivo = null; // Borrar ArchivoModel
    this.myInputVariable.nativeElement.value = '';
  }

  copyText() {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.archivo.downloadURL;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.snackBar.open('Enlace de descarga copiado al portapapeles', '', {
      duration: 2000
    });
  }

  formValid() {
      let sistema_operativo = true;
      if (this.fichero.tipo.toLowerCase() === 'software' && this.fichero.sistemaOperativo.length === 0){
          sistema_operativo = false;
      }

    return (
        (this.fichero.productos.length > 0 ||
        this.categorias_selected_local.length > 0) &&
        this.fichero.titulo !== '' &&
        this.fichero.tipo !== '' &&
        this.fichero.version !== '' &&
        this.fichero.estado !== '' &&
        this.fichero.acceso !== '' &&
        sistema_operativo
    );
  }

  tipoChanged() {
    if (this.fichero.tipo === 'firmware') {
      this.fichero.titulo = 'FW-' + this.fichero.marcas + '-' + this.fichero.version;
    }
  }

  urlProducto(name): string {
    if (this.productos_completo.length > 0) {
      return this.productos_completo.filter(option => option.name === name)[0].url;
    }
  }

  urlCategoria(id): string {
    if (this.categorias_api_web.length > 0) {
      return this.categorias_api_web.filter(option => option.id == id)[0].url;
    }
  }

}

@Component({
        selector: 'dialog-delete-content',
        templateUrl: 'dialog-delete-content.html',
        styleUrls: ['dialog-delete-content.css']
})

export class DialogDeleteContent{}